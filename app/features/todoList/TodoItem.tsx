import React from "react";
import { Text, View, StyleSheet, Button } from "react-native";

type TodoItem = { title: string, description: string, isDone: boolean, remove: () => void, update: () => void }

const TodoItem: React.FC<TodoItem> = ({ title, description, isDone, remove, update }) => {
    return (
        <View style={styles.item}>
            <View style={styles.content}>
                <Text onPress={update} style={[isDone && styles.isDone, styles.title]}>{ title }</Text>
                <Text style={styles.description}>{ description }</Text>
            </View>
            
            <View>
                <Button color={'#dc322f'} onPress={remove} title="X" />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    isDone: {
        textDecorationLine: 'line-through',
    },
    title: {
        fontSize: 24,
        color: '#073642',
    },
    description: {
        fontSize: 20,
        color: '#586e75',
    },
    item: {
        padding: 20,
        flexDirection: 'row',
    },
    content: {
        flexGrow: 1
    }
});

export default TodoItem;