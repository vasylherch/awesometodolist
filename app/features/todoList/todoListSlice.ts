import { Action, createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RootState, store } from '../../store';

const uid = () => {
    return Date.now();
}

export type Todo = {
    title: string
    description: string
    isDone: boolean
    id: number
}

export type TodoListState = {
    todoList: Todo[]
}

const initialState: TodoListState = {
    todoList: []
}

export const storeData = createAsyncThunk(
    'todoList/storeData',
    async function () {
        const list = store.getState().todoList;
        try {
            const jsonValue = JSON.stringify(list);
            await AsyncStorage.setItem('@todo_list', jsonValue);
        } catch (e) {
        }
    }
);

export const getData = createAsyncThunk(
    'todoList/getData',
    async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('@todo_list');
            return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch(e) {
        }
    }
);

export const todoListSlice = createSlice({
    name: 'todoList',
    initialState,
    reducers: {
        markDone: (state, action) => {
            state.todoList.find(item => {
                if (item.id === action.payload) {
                    item.isDone = !item.isDone
                }
            })
        },
        remove: (state, action) => {
            state.todoList = state.todoList.filter(item => item.id !== action.payload)
        },
        removeAll: (state) => {
            state.todoList = [];
        },
        addTodo: (state, action) => {
            const {title, description} = action.payload
            state.todoList.push({
                title,
                description,
                isDone: false,
                id: uid(),
            });
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getData.fulfilled, (state, action) => {
            state.todoList = action.payload?.todoList || [];
        })

        builder.addCase(storeData.pending, (state, action) => {
            console.log('storeData', state)
        })
    }
})

export default todoListSlice.reducer

export const getTodos = (state: RootState) => state.todoList

export const { markDone, remove, addTodo, removeAll } = todoListSlice.actions