import React, { Dispatch, useState } from 'react';
import { Text, View, TextInput, Button } from 'react-native';
import { addTodo } from "./todoListSlice";
import { useAppDispatch } from '../../hooks';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RootStackParamList } from '../../../App';

type Props = NativeStackScreenProps<RootStackParamList>

const CreateTodo = ({ navigation }: Props) => {
    const [title, setTitle] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const dispatch = useAppDispatch();
    const add = () => {
        if (title === '' || description === '') return;
        dispatch(addTodo({title, description}));
        navigation.goBack();
    }

    return (
        <View>
            <Text>Title</Text>
            <Input value={title} onChangeText={setTitle} />
            <Text>Description</Text>
            <Input value={description} onChangeText={setDescription} />
            <Button
                title="Add"
                onPress={() => add()}
            />
        </View>
    );
}

const Input: React.FC<{ value: string, onChangeText: Dispatch<string> }> = ({ value, onChangeText }) => {
    return (
        <TextInput
            style={{
                height: 40,
                margin: 12,
                borderWidth: 1,
                padding: 10,
            }}
            value={value}
            onChangeText={onChangeText}
        />
    )
}

export default CreateTodo;
