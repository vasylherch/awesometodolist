import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useEffect } from "react";
import { TouchableOpacity, Text, View, ScrollView, StyleSheet, Button, SafeAreaView, AppState } from "react-native";
import { RootStackParamList } from "../../../App";
import { useAppSelector, useAppDispatch } from '../../hooks';
import TodoItem from "./TodoItem";
import { markDone, remove, getData, storeData, removeAll } from "./todoListSlice";

type Props = NativeStackScreenProps<RootStackParamList>

const TodoList = ({ navigation }: Props) => {
    const todos = useAppSelector((state) => state.todoList);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(getData());

        const subscription = AppState.addEventListener("change", nextAppState => {
            if (nextAppState === "background") {
                dispatch(storeData());
            }
        });

        return () => { subscription.remove(); };
    }, [dispatch]);

    const removeTodo = (id: number) => {
        dispatch(remove(id));
    }

    const updateTodo = (id: number) => {
        dispatch(markDone(id));
    }

    const clearList = () => {
        dispatch(removeAll());
    }

    return (
        <SafeAreaView style={styles.todoListContainer}>
            <ScrollView>
                {todos.todoList.map((item, index) =>
                    <TodoItem update={() => updateTodo(item.id)} remove={() => removeTodo(item.id)} key={index} title={item.title} description={item.description} isDone={item.isDone} />
                )}
                {todos?.todoList.length > 0 && <View style={styles.clearButtonContainer}><Button
                    color={'#dc322f'}
                    title="Clear todo list"
                    onPress={() => clearList()}
                /></View>}
            </ScrollView>

            <View style={styles.addButtonContainer}><AddButton
                title="Add"
                onPress={() => navigation.navigate('NewTodo')}
            /></View>
            
        </SafeAreaView>

    )
}

const AddButton: React.FC<{ title: string, onPress: () => void }> = ({ title, onPress }) => {
    return (
        <TouchableOpacity onPress={ onPress } style={styles.addButton}>
            <Text style={styles.addButtonTitle}>{ title }</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    addButton: {
        backgroundColor: '#268bd2',
        width: 80,
        height: 80,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    addButtonTitle: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    },
    addButtonContainer: {
        position: 'absolute',
        bottom: 20,
        right: 20,
    },
    todoListContainer: {
        flex: 1,
        backgroundColor: '#fdf6e3',
    },
    clearButtonContainer: {
        width: 150,
        alignSelf: 'center',
        marginTop: 40,
        marginBottom: 40,
    },
});

export default TodoList
