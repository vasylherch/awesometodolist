import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { store } from './app/store';
import Todos from './app/features/todoList/TodoList';
import CreateTodo from './app/features/todoList/CreateTodo';

export type RootStackParamList = {
  TodoList: undefined;
  NewTodo: undefined;
};

const Stack = createNativeStackNavigator<RootStackParamList>();



const App: React.FC = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="TodoList" component={Todos} />
          <Stack.Screen name="NewTodo" component={CreateTodo} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

const styles = StyleSheet.create({

});

export default App;
